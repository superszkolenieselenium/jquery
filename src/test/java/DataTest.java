import org.testng.annotations.Test;

import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class DataTest {
    @Test
    public void test() {

        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("MMMM yyyy", Locale.ENGLISH);
        YearMonth targetDate = YearMonth.parse("January 2054", dateFormat);
        YearMonth actualDate = YearMonth.parse("March 2018", dateFormat);


        System.out.println(targetDate.isAfter(actualDate));
    }

    public boolean compareDate(String targetDate, String actualDate) {
        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("MMMM yyyy", Locale.ENGLISH);
        YearMonth targetDate2 = YearMonth.parse(targetDate, dateFormat);
        YearMonth actualDate2 = YearMonth.parse(actualDate, dateFormat);
        boolean result = targetDate2.isAfter(actualDate2);
        return result;
    }
}
