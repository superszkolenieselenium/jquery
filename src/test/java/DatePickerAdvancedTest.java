import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;

public class DatePickerAdvancedTest extends BaseTest {

    @Test
    public void dataPicker() throws InterruptedException {
        driver.get("https://jqueryui.com/datepicker/#default");

        WebElement iframe = driver.findElement(By.cssSelector("iframe"));
        driver.switchTo().frame(iframe);

        WebElement dateInput = driver.findElement(By.id("datepicker"));
        dateInput.click();
        selectDate(12, "December", 2018);
        Thread.sleep(3000);

        dateInput.click();
        selectDate(27, "February", 2019);
        Thread.sleep(3000);


        dateInput.click();
        selectDate(01, "January", 2019);
        Thread.sleep(3000);


        dateInput.click();
        selectDate(11, "November", 2018);
        Thread.sleep(3000);


        dateInput.click();
        selectDate(11, "November", 2018);
        Thread.sleep(3000);
        Assert.assertEquals(dateInput.getAttribute("value"), "03/03/2019");
    }

    private void selectDate(int targetDay, String targetMonth, int targetYear) throws InterruptedException {
        String dataToSet = targetMonth + " " + targetYear;
        String actualData = getActualData().getText();

        if (convertToDate(dataToSet).isAfter(convertToDate(actualData))) {
            goNext(targetMonth, targetYear);

        } else if (convertToDate(dataToSet).isBefore(convertToDate(actualData))) {
            goPrevious(targetMonth, targetYear);
        }
        selectDay(targetDay);
    }

    private void goNext(String targetMonth, int targetYear) throws InterruptedException {
        do {
            getNext().click();
            Thread.sleep(200);

        } while (!compareDate(targetMonth, targetYear));

    }

    private void goPrevious(String targetMonth, int targetYear) throws InterruptedException {
        do {
            getPrevious().click();
            Thread.sleep(200);
        } while (!compareDate(targetMonth, targetYear));
    }

    private YearMonth convertToDate(String dateToConvert) {
        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("MMMM yyyy", Locale.ENGLISH);
        YearMonth date = YearMonth.parse(dateToConvert, dateFormat);
        return date;
    }

    private boolean compareDate(String targetMonth, int targetYear) {
        String actualMonth = getActualMonth().getText();
        int actualYear = Integer.parseInt(getActualYear().getText());

        if (actualYear == targetYear && targetMonth.equals(actualMonth)) {
            return true;
        }
        return false;
    }

    private void selectDay(int targetDay) {
        List<WebElement> allDays = driver.findElements(By.cssSelector("td[data-handler='selectDay']"));
        for (WebElement day : allDays) {
            if (Integer.parseInt(day.getText()) == targetDay) {
                day.click();
                break;
            }
        }
    }

    private WebElement getActualData() {
        return driver.findElement(By.cssSelector(".ui-datepicker-title"));
    }

    private WebElement getNext() {
        return driver.findElement(By.cssSelector(".ui-datepicker-next"));
    }

    private WebElement getPrevious() {
        return driver.findElement(By.cssSelector(".ui-datepicker-prev"));
    }

    private WebElement getActualMonth() {
        return driver.findElement(By.className("ui-datepicker-month"));
    }

    private WebElement getActualYear() {
        return driver.findElement(By.className("ui-datepicker-year"));
    }


}