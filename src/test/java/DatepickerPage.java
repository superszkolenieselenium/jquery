import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

public class DatepickerPage extends BaseTest {
    String[] months = {"January", "February",
            "March", "April", "May", "June", "July", "August", "September",
            "October", "November", "December"};

    @Test
    public void dataPicker() throws InterruptedException {
        driver.get("https://jqueryui.com/datepicker/#default");

        WebElement iframe = driver.findElement(By.cssSelector("iframe"));
        driver.switchTo().frame(iframe);

        WebElement dateInput = driver.findElement(By.id("datepicker"));
        dateInput.click();
        selectDate(3, "March", 2019);
        Assert.assertEquals(dateInput.getAttribute("value"), "03/03/2019");
    }

    private void selectDate(int targetDay, String targetMonth, int targetYear) throws InterruptedException {
        if (targetYear > Integer.parseInt(getActualYear().getText())) {
            goNext(targetMonth, targetYear);
            selectDay(targetDay);
        } else {
            goPrevious(targetMonth, targetYear);
            selectDay(targetDay);
        }
    }

    private void goNext(String targetMonth, int targetYear) throws InterruptedException {
        do {
            getNext().click();
            Thread.sleep(200);
        } while (!compareDate(targetMonth, targetYear));

    }

    private void goPrevious(String targetMonth, int targetYear) throws InterruptedException {
        do {
            getPrevious().click();
            Thread.sleep(200);
        } while (!compareDate(targetMonth, targetYear));
    }

    //                                  March           2019
    private boolean compareDate(String targetMonth, int targetYear) {
        String actualMonth = getActualMonth().getText();
        int actualYear = Integer.parseInt(getActualYear().getText());

        if (actualYear == targetYear && targetMonth.equals(actualMonth)) {
            return true;
        }
        return false;
    }

    private void selectDay(int targetDay) {
        List<WebElement> allDays = driver.findElements(By.cssSelector("td[data-handler='selectDay']"));
        for (WebElement day : allDays) {
            if (Integer.parseInt(day.getText()) == targetDay) {
                day.click();
                break;
            }
        }
    }

    private WebElement getNext() {
        return driver.findElement(By.cssSelector(".ui-datepicker-next"));
    }

    private WebElement getPrevious() {
        return driver.findElement(By.cssSelector(".ui-datepicker-prev"));
    }

    private WebElement getActualMonth() {
        return driver.findElement(By.className("ui-datepicker-month"));
    }

    private WebElement getActualYear() {
        return driver.findElement(By.className("ui-datepicker-year"));
    }


}