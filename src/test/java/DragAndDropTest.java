import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.Test;

public class DragAndDropTest extends BaseTest {
    @Test
    public void simpleDragAndDropTest() {
        driver.get("https://jqueryui.com/droppable/");
        WebElement iframe = driver.findElement(By.cssSelector("iframe"));
        driver.switchTo().frame(iframe);

        WebElement toDrag = driver.findElement(By.id("draggable"));
        WebElement placeToDrop = driver.findElement(By.id("droppable"));

        Actions actionBuilder = new Actions(driver);
        Action action = actionBuilder.dragAndDrop(toDrag, placeToDrop).build();
        action.perform();
        Assert.assertEquals(placeToDrop.getText(), "Dropped!");

    }

    @Test
    public void dragAndDropTest() throws InterruptedException {
        driver.get("https://jqueryui.com/droppable/");
        WebElement iframe = driver.findElement(By.cssSelector("iframe"));
        driver.switchTo().frame(iframe);

        WebElement toDrag = driver.findElement(By.id("draggable"));
        WebElement placeToDrop = driver.findElement(By.id("droppable"));

        Actions actionBuilder = new Actions(driver);
        Action action = actionBuilder.moveToElement(toDrag)
                .clickAndHold(toDrag)
                .moveToElement(placeToDrop)
                .release()
                .build();
        action.perform();
        Assert.assertEquals(placeToDrop.getText(), "Dropped!");
    }
}
