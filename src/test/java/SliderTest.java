import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SliderTest extends BaseTest {
    @Test
    public void sliderTest() throws InterruptedException {
        int[] positions = {50, 20, 35, 35};
        driver.get("https://jqueryui.com/slider/#custom-handle");
        WebElement iframe = driver.findElement(By.cssSelector("iframe"));
        driver.switchTo().frame(iframe);
        WebElement slider = driver.findElement(By.id("custom-handle"));

        for (int position : positions) {
            moveTo(slider, position);
            Thread.sleep(1000);
            Assert.assertEquals(Integer.parseInt(slider.getText()), position);
        }
    }

    private void moveTo(WebElement slider, int targetPosition) {
        int actualPosition = Integer.parseInt(slider.getText());
        int move = Math.abs(targetPosition - actualPosition);

        if (actualPosition > targetPosition) {
            for (int i = 0; i < move; i++) {
                slider.sendKeys(Keys.ARROW_LEFT);
            }
        } else {
            for (int i = 0; i < move; i++) {
                slider.sendKeys(Keys.ARROW_RIGHT);
            }
        }
    }
}
